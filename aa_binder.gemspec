# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'aa_binder/version'

Gem::Specification.new do |spec|
  spec.name          = "aa_binder"
  spec.version       = AaBinder::VERSION
  spec.authors       = "Tom"
  spec.email         = ["anonymous@motevets.com"]

  spec.summary       = "Download, combine, and label chapters for Alcoholics Anonymous literature."
  spec.description   = `cat README.md`
  spec.homepage      = "https://gitlab.com/motevets/aa_binder"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
